<?php

/**
 * @file media_vbox7/media_vbox7.module
 *
 * Media: Vbox7 provides a stream wrapper and formatters for videos provided
 * by Vbox7.com, available at http://vbox7.com/.
 */

// A registry of variable_get defaults.
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'media_vbox7') . '/includes/media_vbox7.variables.inc';

// Hooks and callbacks for integrating with Styles module for display.
// @todo Can save a little overhead for people without Styles module by wrapping
//   this inside a module_exists('styles'). However, is that safe to do in
//   global context? If not, is there any down side to doing it in hook_init()?
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'media_vbox7') . '/includes/media_vbox7.styles.inc';

// Hooks and callbacks for integrating with File Entity module for display.
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'media_vbox7') . '/includes/media_vbox7.formatters.inc';

/**
 * Implements hook_media_internet_providers().
 */
function media_vbox7_media_internet_providers() {
  $path = drupal_get_path('module', 'media_vbox7');
  return array(
    'MediaInternetVbox7Handler' => array(
      'title' => 'vbox7.com',
      'image' => $path . '/images/stream-vbox7.png'
    ),
  );
}

/**
 * Implements hook_stream_wrappers().
 */
function media_vbox7_stream_wrappers() {
  return array(
    'vbox7' => array(
      'name' => t('Vbox7 videos'),
      'class' => 'MediaVbox7StreamWrapper',
      'description' => t('Videos provided by Vbox7.'),
      'type' => STREAM_WRAPPERS_READ_VISIBLE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function media_vbox7_theme($existing, $type, $theme, $path) {
  return array(
    'media_vbox7_preview_style' => array(
      'variables' => array('style_name' => NULL),
      'file' => 'media_vbox7.theme.inc',
      'path' => $path . '/includes/themes',
    ),
    'media_vbox7_field_formatter_styles' => array(
      'variables' => array('element' => NULL, 'style' => NULL),
      'file' => 'media_vbox7.theme.inc',
      'path' => $path . '/includes/themes',
    ),
    'media_vbox7_video' => array(
      'variables' => array('uri' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL, 'fullscreen' => NULL),
      'file' => 'media_vbox7.theme.inc',
      'path' => $path . '/includes/themes',
      'template' => 'media-vbox7-video',
    ),
    'media_vbox7_embed' => array(
      'variables' => array('style_name' => NULL, 'uri' => NULL, 'alt' => NULL, 'title' => NULL),
      'file' => 'media_vbox7.theme.inc',
      'path' => $path . '/includes/themes',
    ),
    'media_vbox7_styles' => array(
      'variables' => array('element' => NULL, 'style' => NULL),
      'file' => 'media_vbox7.theme.inc',
      'path' => $path . '/includes/themes',
    ),
  );
}

/**
 * Implements hook_media_parse().
 *
 * @todo This hook should be deprecated. Refactor Media module to not call it
 *   any more, since media_internet should be able to automatically route to the
 *   appropriate handler.
 */
function media_vbox7_media_parse($embed_code) {
  $handler = new MediaInternetVbox7Handler($embed_code);
  return $handler->parse($embed_code);
}

/**
 * Implements hook_media_format_form_prepare_alter().
 */
function media_vbox7_media_format_form_prepare_alter(&$form, &$form_state, $media) {
  $settings = array('autosubmit' => ($media->type == "video"));
  drupal_add_js(array('media_format_form' => $settings), 'setting');
}

/**
 * Implements hook_ctools_plugin_api().
 */
function media_vbox7_ctools_plugin_api($owner, $api) {
  static $api_versions = array(
    'file_entity' => array(
      'file_default_displays' => 1,
    ),
  );
  if (isset($api_versions[$owner][$api])) {
    return array('version' => $api_versions[$owner][$api]);
  }
}
