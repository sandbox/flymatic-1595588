<?php

/**
 *  @file
 *  Create a Vbox7 Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $youtube = new ResourceYouTubeStreamWrapper('youtube://?v=[video-code]');
 */
class MediaVbox7StreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://vbox7.com/play:';

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/vbox7';
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    return 'http://i.vbox7.com/p/'. check_plain($parts['v']) .'4.jpg';
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-vbox7/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}
