<?php

/**
 * @file media_vbox7/includes/themes/media-vbox7-video.tpl.php
 *
 * Template file for theme('media_vbox7_video').
 *
 * Variables available:
 *  $uri - The uri to the Vbox7 video, such as vbox://v/xsy7x8c9.
 *  $video_id - The unique identifier of the Vbox7 video.
 *  $width - The width to render.
 *  $height - The height to render.
 *  $autoplay - If TRUE, then start the player automatically when displaying.
 *  $fullscreen - Whether to allow fullscreen playback.
 *
 * Note that we set the width & height of the outer wrapper manually so that
 * the JS will respect that when resizing later.
 */
?>
<div class="media-vbox7-outer-wrapper" id="media-vbox7-<?php print $id; ?>" style="width: <?php print $width; ?>px; height: <?php print $height; ?>px;">
  <div class="media-vbox7-preview-wrapper" id="<?php print $wrapper_id; ?>">
    <?php print $output; ?>
  </div>
</div>
