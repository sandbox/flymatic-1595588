<?php

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetVbox7Handler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    // Vbox7 has a  URL format:
    //   http://vbox7.com/play:*
    //   \[play:([a-z0-9]{8})(:[1-9][0-9]{1,2})?(:[1-9][0-9]{1,2})?(:1)?\]/
		// '@vbox7\.com/play\:([a-z0-9]+)/i',
		//'@vbox7\.com/play:([a-z0-9]{8})(:[1-9][0-9]{1,2})?(:[1-9][0-9]{1,2})?(:1)?/i',
		
    $patterns = array(
      
			'@vbox7\.com/play\:([a-z0-9]+)@i',
			
		);
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1])) {
        return file_stream_wrapper_uri_normalize('vbox7://v/' . $matches[1]);
      }
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function save() {
    $file = $this->getFileObject();
    file_save($file);
    return $file;
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    //@todo: this is terribly broken in some ways because the function is really
    // made for local files which are 'real'
    $file = file_uri_to_object($uri, TRUE);

  
    return $file;
  }
}


